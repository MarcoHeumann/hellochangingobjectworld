package sample.models;

import javafx.scene.paint.Paint;
import javafx.scene.text.Font;

/**
 * This class represents a message object.
 * It also keeps any logic for custom outputs of its contents.
 */
public class Message {
    //Variables
    public String messageOne;
    public String messageTwo;
    public String messageColor;
    public double messageSize;

    /**
     * Constructor
     * @param m1 Message one
     * @param m2 Message two
     * @param color Font Color for message two
     * @param size Font size for message two
     */
    public Message(String m1, String m2, String color, int size){
        messageOne = m1;
        messageTwo = m2;
        messageColor = color;
        messageSize = size;
    }

    /**
     * Returns message two for databinding.
     * Checks if the text should be inverted first.
     * @param invert Value to check against for inverting
     * @return Message two, possibly inverted
     */
    public String getTextFlow(Boolean invert) {
        if(!invert) {
            return messageTwo;
        }
        else {
            StringBuilder builder = new StringBuilder(messageTwo);
            return builder.reverse().toString();
        }
    }

    /**
     * Mathod that checks if a custom color is set.
     * @return Black or custom color
     */
    public Paint getColor() {
        if(messageColor != null) {
            return Paint.valueOf(messageColor);
        }
        else {
            return Paint.valueOf("Black");
        }
    }

    /**
     * Method that changes font size to custom values.
     * @return new Font(size)
     */
    public Font getFontSize() {
        return new Font(messageSize);
    }
}
