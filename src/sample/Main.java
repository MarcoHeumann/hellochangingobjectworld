package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Main class and entry point for the application.
 */
public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        //create a root and load fxml code
        Parent root = FXMLLoader.load(getClass().getResource("views/HelloView.fxml"));
        //add root to the window and show it
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    /**
     * This method is called when the program is started.
     * @param args Possible arguments here: none
     */
    public static void main(String[] args) {
        launch(args);
    }
}
