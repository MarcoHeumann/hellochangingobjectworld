package sample.viewmodels;

import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import sample.models.Message;

import java.util.ArrayList;
import java.util.List;

/**
 * ViewModel / Controller class for the HelloView.
 */
public class HelloViewModel {
    //FXML Variables
    @FXML
    private Label messageLabel;
    private SimpleStringProperty labelOneText = new SimpleStringProperty();

    @FXML
    private Label messageLabelTwo;
    private SimpleStringProperty labelTwoText = new SimpleStringProperty();

    //basic Variables
    private List<Message> myMessages = new ArrayList<Message>();
    private int currentIndex = 0;
    private Message currentMessage;
    private Boolean inverted = false;

    /**
     * Only adds a bunch of messages. Outsourced for cleaner looking code.
     */
    private void addMessages() {
        myMessages.add(new Message("Hello object World!", "Is this working?", "Green", 24));
        myMessages.add(new Message("Hello small object World!?", "It is indeed working!", "Blue", 14));
        myMessages.add(new Message("A null value for color!", "Still running", null, 24));
        myMessages.add(new Message("Gieve me,...", "...MOAR! (and bigger)", "Red", 50));
    }

    /**
     * Automagically called after construction of the class. Some things can't be done before this, so use it.
     */
    @FXML
    public void initialize() {
        //Databinding for labels
        messageLabel.textProperty().bind(labelOneText);
        messageLabelTwo.textProperty().bind(labelTwoText);

        //add messages
        addMessages();

        //update view
        updateMyStuff();
    }

    /**
     * Used to update all the values that are bound to the view.
     * Can also adjust settings like colors the hard way, but you could find a way to bind the color values in a
     * prettier way!
     */
    private void updateMyStuff() {
        currentMessage = myMessages.get(currentIndex);

        labelOneText.set(currentMessage.messageOne);
        labelTwoText.set(currentMessage.getTextFlow(inverted));
        messageLabelTwo.textFillProperty().setValue(currentMessage.getColor());
        messageLabelTwo.setFont(currentMessage.getFontSize());
    }

    /**
     * Called by a view button.
     * Switches index for currently selected message and triggers an update.
     */
    @FXML
    private void showNextMessage() {
        if(currentIndex < myMessages.size() - 1) {
            currentIndex++;
        }
        else {
            currentIndex = 0;
        }
        updateMyStuff();
    }

    /**
     * Called by a view button.
     * Toggles inverting text on and off.
     */
    @FXML
    private void invertIt(){
        inverted = !inverted;
        updateMyStuff();
    }
}
