module HelloChangingObjectWorld {
    requires javafx.fxml;
    requires javafx.controls;

    //Don't forget to add any package that needs access!
    opens sample;
    opens sample.viewmodels;
}