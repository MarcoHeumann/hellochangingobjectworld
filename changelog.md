##Version 2.2 - The glory of Java-doc!
###New Features
* Java doc. Plenty.

###Changes
none

###Bug fixes
none

###Open issues
none


##Version 2.1 - MVVM Cleanup!
###New Features
none

###Changes
* Messages now calculate all their own stuff. No external logic anymore.
* Packages are being used now.
* MVVM like renaming of files.
* Minor code cleanup in controller class.
* New packages added to module-info.

###Bug fixes
none

###Open issues
* How could anyone understand this, without comments,...


##Version 2.0 - Here we go!
###New Features
* Messages now have support for text color and size.
* Messages can invert their text on demand.

###Changes
* Adjusted Button sizes in order to make them look more epic(ish).

###Bug fixes
none

###Open issues
none


##Version 1.3 - More messages, as objects!
###New Features
* Messages are now object based.
* Button now has the expected functionality.

###Changes
* Replaced message strings with a list of message objects.
* Moved bind updating logic into a reusable method.

###Bug fixes
none

###Open issues
none


##Version 1.2 - More Text, with magic!
###New Features
* A second text line is showing up on a new label.
* Button added to show other messages.

###Changes
* Removed unwanted big spacing before first label.
* Removed unneeded extra columns.
* Increased Label size for easier reading.

###Bug fixes
none

###Open issues
* Button does not yet have any function.
* No multi message support yet.
* Not any less boring.


##Version 1.1 - Custom Text!
###New Features
* Message is now displayed as a label.

###Changes
* Adjusted window size to be wider by default.

###Bug fixes
none

###Open issues
* Adding multiple messages is not yet supported.
* Still boring.


##Version 1 - It runs!
###New Features
* JavaFX added to project template via [this tutorial](https://www.youtube.com/watch?v=WtOgoomDewo).
* Hello World window now opens up and looks shiny!

###Changes
none

###Bug fixes
* Missing module-info no longer stops the program from starting.

###Open issues
* It's boring. :-(


##Version 0.1 - It burns!
###New Features
* Initial program created.

###Changes
none

###Bug fixes
none

###Open issues
* Program does not start.